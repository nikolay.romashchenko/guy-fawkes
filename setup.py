from setuptools import setup


setup(
    name='guyfawkes',
    version='0.1',
    install_requires=[
        'Flask==1.0.2.',
    ],
    entry_points='''
        [console_scripts]
        guyfawkes=guyfawkes:cli
    ''',
)